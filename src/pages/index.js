import React from "react"
import Container from "../components/Container"
import Header from "../components/Header"
import Nav from "../components/Nav"

export default function Home() {
  return (
    <Container>
      <Nav />
      <Header text="Main title" />
      <p>Hello there!</p>
    </Container>
  )
}
