import React from 'react';
import Container from '../components/Container';
import Header from '../components/Header';
import Nav from '../components/Nav';

const Contact = () => (
  <Container>
    <Nav />
    <Header text="Contact me" />
    <p>
      Don't hesitate !
    </p>
  </Container>
);

export default Contact;