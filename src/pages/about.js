import React from 'react';
import Container from '../components/Container';
import Header from '../components/Header';
import Nav from '../components/Nav';

const About = () => (
  <Container>
    <Nav />
    <Header text="About" />
    <p>This is the about page</p>
  </Container>
);

export default About;
